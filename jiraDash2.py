import json 
import sys
import urllib
import urllib3
from urllib.request import urlopen
from httplib2 import Http
from json import dumps
from dateutil.parser import parse
from datetime import timedelta, date
import datetime
from jira import JIRA

CodigoBoard= 2407
hangoutsChatUrlAtendimento = 'https://chat.googleapis.com/v1/spaces/AAAAFuru9hE/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=7431HKjiaPYicu2I9U69vplEajDMTFurX9q8msnELl4%3D'

jira = JIRA(basic_auth=('roberto.arruda', 'EscolaEstadual2'), options = {'server': 'http://jiraproducao.totvs.com.br'})

issues_in_project = jira.search_issues('project=DSAUATE AND SPRINT not in closedSprints() AND sprint not in futureSprints()')

''' Resgatar o ID da Board

Atendiemnto: 2407
boards = jira.boards()
board = [b for b in boards if b.id  == "Board PLS Saúde - Atendimento"][0]

print(board.id)
'''

sprints = jira.sprints(CodigoBoard)
totalRecusada = 0
totalConcluida = 0
totalCancelado = 0
totalBacklog = 0
totalDesenv = 0
total = 0
sprint_name = ""
qtdAnalista = sys.argv[1] 
qtdDias = sys.argv[2] 


for sprint in sorted([s for s in sprints if s.raw[u'state'] == u'ACTIVE'], key = lambda x: x.raw[u'sequence']):
	if(sprint.id != 593): #Retira sprint MSAU que resgata indevidamente no filtro.
	
		issues_in_project = jira.search_issues('issuetype in (Story, Manutenção, Legislação, Apoio, "Apoio - Cliente", "Documentação") AND project=DSAUATE AND SPRINT = ' + str(sprint.id))
		sprint_name = sprint.name
		
		for value in issues_in_project:
			status = str(value.fields.status)[:30]
			if (value.fields.customfield_10006 != None):				
				total += value.fields.customfield_10006
				if(status.upper() == "RECUSADA"):
					totalRecusada += value.fields.customfield_10006
				elif(status.upper() == "CONCLUÍDO"):
					totalConcluida += value.fields.customfield_10006
				elif(status.upper() == "CANCELADO"):
					totalCancelado += value.fields.customfield_10006
				elif(status.upper() == "BACKLOG"):	
					totalBacklog += value.fields.customfield_10006
				else:
					totalDesenv += value.fields.customfield_10006
					
				#print(status)
			#print(value.key, value.fields.customfield_10006)
			#print(value.key , value.fields.summary , value.fields.assignee , value.fields.reporter ,value.fields.updated ,value.fields.resolutiondate)
		
#Estimando Sprint
format_str = '%d/%m/%Y' 
pos1 = sprint_name.find("(");
pos2 = sprint_name.find(")");

data_atual = datetime.datetime.strptime(date.today().strftime("%d/%m/%Y"), format_str)


dataInicio = datetime.datetime.strptime(sprint_name[pos1+1:pos1 + 6] + "/" + str(data_atual.year), format_str) 
dataFim =    datetime.datetime.strptime(sprint_name[pos2 - 5:pos2] + "/" + str(data_atual.year), format_str)

dataInicio = datetime.datetime.strptime("11/03/2019", format_str)

quantidade_dias = (data_atual - dataInicio).days

if(quantidade_dias >= 5):
	quantidade_dias -= 3 #Tira dois dias do final de semana e tira a segunda-feira - Planning
else:
	quantidade_dias -= 1 #Tira a segunda-feira - Planning


nTotalPontosCorretos = quantidade_dias * float(qtdAnalista)
nTotalPontosAtual    = totalRecusada + totalConcluida + totalCancelado

cMensagem = "\n*_Resumo da Sprint_*\n\n"

cMensagem  += "Estamos na Sprint *" + sprint_name + "*, nessa sprint temos planejados um total de *" + str(total) + " pontos*. Nossa capacidade é de *"+ str(float(qtdAnalista) * float(qtdDias)) +" pontos*.\n\n*Até o momento temos:*\n\n*Pendentes(Em Pontos):* "+str(totalBacklog)+ "\n*Em Andamento(Em Pontos):* " +str(totalDesenv)+ "\n*Finalizadas(Em Pontos):* " + str(totalRecusada + totalConcluida + totalCancelado)
#cMensagem += "\nCapacidade Sprint: " + str(float(qtdAnalista) * float(qtdDias))
#cMensagem += "\n\n*_Resumo_*"

if(nTotalPontosAtual >= nTotalPontosCorretos):
	cMensagem += "\n\n*Parabéns Equipe, estamos no caminho certo.*"
else:
	cMensagem += "\n\n*Atenção, precisamos entrar em linha com o planejado.*"
	
cMensagem += "\nAté o final do dia de ontem, deveríamos ter produzido *" + str(nTotalPontosCorretos) + " pontos*, contudo , produzimos *" + str(nTotalPontosAtual) + " pontos*." 

if(nTotalPontosAtual < nTotalPontosCorretos):
	cMensagem += "\n\nEstamos devendo *" +str(nTotalPontosCorretos - nTotalPontosAtual)+ " pontos*."

print(cMensagem)


messageHeaders = {'Content-Type': 'application/json; charset=UTF-8'}
httpObj = Http()


botMessage = {'text': cMensagem}


response = httpObj.request(
    uri=hangoutsChatUrlAtendimento,
    method='POST',
    headers=messageHeaders,
    body=dumps(botMessage)
)
		
#print (dataInicio.date(), data_atual.date(), quantidade_dias, nTotalPontosCorretos, nTotalPontosAtual)

'''
sprints = jira.sprints(board.id)

for sprint in sorted([s for s in sprints if s.raw[u'state'] == u'ACTIVE'], key = lambda x: x.raw[u'sequence']):
	print(sprint.name)

for jira_sprint in jira_sprints:
	sprint_info = jira.sprint_info('project=DSAUATE', jira_sprint.id)
	print(sprint_info['name'])
		
for value in issues_in_project:
	print(value.sprint, value.key , value.fields.summary , value.fields.assignee , value.fields.reporter ,value.fields.updated ,value.fields.resolutiondate)
'''
	
'''
messageComplete = ""
messageSaudacao = "\n*_Bom dia Equipe de Atendimento. Tenham todos um ótimo dia de Trabalho !_*\n"

messageHeaders = {'Content-Type': 'application/json; charset=UTF-8'}
httpObj = Http()


list = jira.search_issues("project=DSAUATE AND issuetype in (Manutenção) AND status not in (Concluído, Cancelado, Rejected, Recusada, 'Teste Integrado Concluído') order by createdDate", startAt=0, maxResults=500, validate_query=True, fields=None, expand=None, json_result=None)

nCont = 0
cliente = ""
issuekey = ""
dtAcordo = ""
dtCriacao = ""
prioridade = ""
messageCabec = "*Issue\t\t\t\tCriação\t\t\tAcordo\t\t\tPrioridade\tResponsável\t\tCliente*\n"
messageISSUE = ""
responsavel = ""
status = ""
descricao = ""

for item in list:
    nCont += 1 
    issuekey = str(item)
    if(item.fields.customfield_11038.value.strip() == "TEZBAC"):
        cliente = "E-vida \t"
    elif (item.fields.customfield_11038.value.strip() == "T09947"):
        cliente = "Vale \t"
    elif (item.fields.customfield_11038.value.strip() == "T60519"):
        cliente = "Capesesp"        
    else:
        cliente = item.fields.customfield_11071

    try:
        cliente = cliente.ljust(10)
    except:
        cliente = ""

    
    if(item.fields.customfield_11039 == None):
        dtAcordo = "  /  /    \t"
    else:
        dtAcordo = format(parse(item.fields.customfield_11039), "%d/%m/%Y") 
    
    dtCriacao = format(parse(item.fields.created), "%d/%m/%Y")

    prioridade = str(item.fields.priority).rjust(5) 

    if(str(item.fields.assignee) == "None"):
        responsavel = "--------------------".rjust(11)
    else:
        responsavel = str(item.fields.assignee)[:11]

        if("Roberto" in responsavel):
            responsavel = "--------------------".rjust(11)

    status = str(item.fields.status)[:30]
    descricao = str(item.fields.summary)[:100]

    #print(descricao)

    messageISSUE += issuekey + "\t\t" + dtCriacao + "\t\t" + dtAcordo + "\t\t" + prioridade + "\t\t" + responsavel + "\t\t" + cliente + "\t\t" + status + "\n" + descricao + "\n\n"


messageItemBacklog = "\nTemos em nosso Backlog " + str(nCont) + " issues em aberto. São elas:\n\n"

messageComplete = messageSaudacao + messageItemBacklog + messageCabec + messageISSUE

#print(messageISSUE)
    #print(item.fields.reporter) 
    
    

print (messageComplete)


botMessage = {'text': messageComplete}


response = httpObj.request(
    uri=hangoutsChatUrlAtendimento,
    method='POST',
    headers=messageHeaders,
    body=dumps(botMessage)
)

#jira.add_comment('DSAUATE-2956', 'Teste Jenkins') '''
                    