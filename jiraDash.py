import json 
import sys
import urllib
import urllib3
from urllib.request import urlopen
from httplib2 import Http
from json import dumps
from dateutil.parser import parse

from jira import JIRA

hangoutsChatUrlAtendimento = 'https://chat.googleapis.com/v1/spaces/AAAAFuru9hE/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=7431HKjiaPYicu2I9U69vplEajDMTFurX9q8msnELl4%3D'

jira = JIRA(basic_auth=('roberto.arruda', 'EscolaEstadual2'), options = {'server': 'http://jiraproducao.totvs.com.br'})
issue = jira.issue('DSAUATE-2949')

messageComplete = ""
messageSaudacao = "\n*_Bom dia Equipe de Atendimento. Tenham todos um ótimo dia de Trabalho !_*\n"

messageHeaders = {'Content-Type': 'application/json; charset=UTF-8'}
httpObj = Http()


list = jira.search_issues("project=DSAUATE AND issuetype in (Manutenção) AND status not in (Concluído, Cancelado, Rejected, Recusada, 'Teste Integrado Concluído') order by createdDate", startAt=0, maxResults=500, validate_query=True, fields=None, expand=None, json_result=None)

nCont = 0
cliente = ""
issuekey = ""
dtAcordo = ""
dtCriacao = ""
prioridade = ""
messageCabec = "*Issue\t\t\t\tCriação\t\t\tAcordo\t\t\tPrioridade\tResponsável\t\tCliente*\n"
messageISSUE = ""
responsavel = ""
status = ""
descricao = ""

for item in list:
    nCont += 1 
    issuekey = str(item)
    if(item.fields.customfield_11038.value.strip() == "TEZBAC"):
        cliente = "E-vida \t"
    elif (item.fields.customfield_11038.value.strip() == "T09947"):
        cliente = "Vale \t"
    elif (item.fields.customfield_11038.value.strip() == "T60519"):
        cliente = "Capesesp"        
    else:
        cliente = item.fields.customfield_11071

    try:
        cliente = cliente.ljust(10)
    except:
        cliente = ""

    
    if(item.fields.customfield_11039 == None):
        dtAcordo = "  /  /    \t"
    else:
        dtAcordo = format(parse(item.fields.customfield_11039), "%d/%m/%Y") 
    
    dtCriacao = format(parse(item.fields.created), "%d/%m/%Y")

    prioridade = str(item.fields.priority).rjust(5) 

    if(str(item.fields.assignee) == "None"):
        responsavel = "--------------------".rjust(11)
    else:
        responsavel = str(item.fields.assignee)[:11]

        if("Roberto" in responsavel):
            responsavel = "--------------------".rjust(11)

    status = str(item.fields.status)[:30]
    descricao = str(item.fields.summary)[:100]

    #print(descricao)

    messageISSUE += issuekey + "\t\t" + dtCriacao + "\t\t" + dtAcordo + "\t\t" + prioridade + "\t\t" + responsavel + "\t\t" + cliente + "\t\t" + status + "\n" + descricao + "\n\n"


messageItemBacklog = "\nTemos em nosso Backlog " + str(nCont) + " issues em aberto. São elas:\n\n"

messageComplete = messageSaudacao + messageItemBacklog + messageCabec + messageISSUE

#print(messageISSUE)
    #print(item.fields.reporter) 
    
    
'''print(item.fields.project.key) 
print(item.fields.issuetype.id) 
print(item.fields.reporter.displayName)'''

print (messageComplete)


botMessage = {'text': messageComplete}


response = httpObj.request(
    uri=hangoutsChatUrlAtendimento,
    method='POST',
    headers=messageHeaders,
    body=dumps(botMessage)
)

#jira.add_comment('DSAUATE-2956', 'Teste Jenkins') 
                    